import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'material-escolar',
  templateUrl: './material-escolar.component.html',
  styleUrls: ['./material-escolar.component.css']
})
export class MaterialEscolarComponent implements OnInit {

  form!: FormGroup;
  checked: boolean = false;
  form1!: FormGroup;
  lista!: string;
  entra: string[] = [];
  lista2!: string;

  constructor(
    private fb: FormBuilder

  ) { 
    this.crearFormulario();
  }

  ngOnInit(): void {
  }


  crearFormulario(): void {
    this.form = this.fb.group(
      {
        id: [''],
        utiles: this.fb.array([[this.form1 = this.fb.group({
          aceptar: ['', [Validators.requiredTrue]],
          nombre: ['', [Validators.requiredTrue,Validators.pattern('[^A-Za-z\s]|\s{2,}')]],
        })
        ]]),
      }
    )
    console.log(this.form);
    
  }

  get utilesEscolares () {
    return this.form.get('utiles') as FormArray;
  }

  agregarUtil(): void {
    this.form1.value.nombre = '';
    this.utilesEscolares.push(this.fb.control(''));
  }

  eliminarUtil(id: number): void {
    this.utilesEscolares.removeAt(id)
  }

  eliminarUtiles(): void {
    this.form = this.fb.group(
      {
        utiles: this.fb.array([])
      }
    )
  }

 guardarCajas(): void {
    if (this.form1.value.aceptar === true) {
      this.lista = this.form1.value.nombre;
      this.entra.push(this.lista)
      this.lista2 = this.entra.join('\n')
    }
  }

  limpiarCajas(): void { 
    // volvemos el valor de los input vacias  
   this.form1.reset({
     nombre: ''
   })
   this.lista2 = '';
  }
}
